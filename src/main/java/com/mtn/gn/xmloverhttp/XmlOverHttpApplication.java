package com.mtn.gn.xmloverhttp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XmlOverHttpApplication {

	public static void main(String[] args) {
		SpringApplication.run(XmlOverHttpApplication.class, args);
	}

}
